

window.onload = function() {
  getAnalyticsReport();
}


function getAnalyticsReport() {
  var http = new XMLHttpRequest();
  var url = "https://brandoncunningham.dev/analytics/report"

  http.open("GET", url, true);
  http.setRequestHeader("sessionId", createOrGetSessionId());
  http.setRequestHeader("userId", createOrGetUserId());
  http.send();


  http.onreadystatechange = (response) => {
    if (http.readyState === XMLHttpRequest.DONE) {
      if(http.status === 200) {
          var response = JSON.parse(http.response);
          console.log(response);
          updateCharts(response);
          updateKeyStats(response);
          updateYourActions(response);
          updateAllActions(response);
          updateDisplay();
      } else {
          document.getElementById("loading-icon").style.display = 'none';
          document.getElementById("loading-failed").style.display = null;
      }
    }
  }
}

function updateDisplay() {
  document.getElementById("live-data").style.display = null;
  document.getElementById("white-bar").style.display = null;
  document.getElementById("more-data").style.display = null;
  document.getElementById("loading-icon").style.display = 'none';

}

function updateAllActions(response) {
  let allActionsTable = document.getElementById("totalActionsTable");

  for (let i = 0; i < response.totalActions.length; i++) {
    var rowNode = document.createElement("tr");
    var action = document.createElement("td");
    var total = document.createElement("td");
    var user = document.createElement("td");
    var session = document.createElement("td");

    action.innerText = response.totalActions[i].action;
    total.innerText = response.totalActions[i].totalActions;
    user.innerText = response.totalActions[i].userPercentage+"%";
    session.innerText = response.totalActions[i].sessionPercentage+"%";


    rowNode.appendChild(action);
    rowNode.appendChild(total);
    rowNode.appendChild(user);
    rowNode.appendChild(session);
    allActionsTable.appendChild(rowNode);
  }
}

function updateYourActions(response) {
  let yourTable = document.getElementById("yourActionsTable");

  for (let i = 0; i < response.yourActions.length; i++) {
    var rowNode = document.createElement("tr");
    var action = document.createElement("td");
    var time = document.createElement("td");

    action.innerText = response.yourActions[i].action;
    time.innerText = response.yourActions[i].time;
    rowNode.appendChild(action);
    rowNode.appendChild(time);
    yourTable.appendChild(rowNode);
  }
}

function updateKeyStats(response) {
  document.getElementById("activeUsers").innerText = response.currentActiveUsers;
  document.getElementById("activeSessions").innerText = response.currentActiveSessions;
  document.getElementById("totalPageLoads").innerText = response.totalPageLoads;
  document.getElementById("totalUsers").innerText = response.totalUniqueUsers;
  document.getElementById("averageVisit").innerText = response.averageVisit;

}


function updateCharts(response) {
    barColors = [];
    for (let i = 0; i < response.dailyChartLabels.length; i++) {
      barColors.push("#709A24");
    }

    dailyVisitChart.data.labels = response.dailyChartLabels;
    dailyVisitChart.data.datasets[0].data = response.dailyViews;
    dailyVisitChart.data.datasets[0].backgroundColor = barColors;
    dailyVisitChart.update();

    dailyUserChart.data.labels = response.dailyChartLabels;
    dailyUserChart.data.datasets[0].data = response.dailyUsers;
    dailyUserChart.data.datasets[0].backgroundColor = barColors;
    dailyUserChart.update();
}










function createOrGetSessionId() {
  return localStorage.getItem("sessionId");
}

function createOrGetUserId() {
  return localStorage.getItem("userId");
}


var dailyVisitChart = new Chart("myChart", {
  type: "bar",
  data: {
    labels: [],
    datasets: [{
      backgroundColor: [],
      data: []
    }]
  },
  options: {
    legend: {display: false},
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 16,
            }
        }],
        xAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 16,
            }
        }]
    }
  }
});

var dailyUserChart = new Chart("myChart1", {
  type: "bar",
  data: {
    labels: [],
    datasets: [{
      backgroundColor: [],
      data: []
    }]
  },
  options: {
    legend: {display: false},
    scales: {
        yAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 16,
            }
        }],
        xAxes: [{
            ticks: {
                fontColor: "white",
                fontSize: 16,
            }
        }]
    }
  }
});