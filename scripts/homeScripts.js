
//UI Stuff
const maxSizeForSidebar = window.matchMedia('(max-width: 1200px)')
const minSizeForSidebar = window.matchMedia('(min-width: 1200px)');

function clickExpand(id) {
	console.log(id); 
	document.getElementById(id+"-expand").style.display = "none";
	document.getElementById(id+"-primary").style.display = "none";
	document.getElementById(id+"-minimize").style.display = null;
	document.getElementById(id+"-secondary").style.display = null;
	createEvent("EXPAND-"+id);
}

function clickMinimize(id) {
	document.getElementById(id+"-expand").style.display = null;
	document.getElementById(id+"-primary").style.display = null;
	document.getElementById(id+"-minimize").style.display = "none";
	document.getElementById(id+"-secondary").style.display = "none";
	createEvent("MINIMIZE-"+id);
}

function expandMenu() {
	let value = window.getComputedStyle(document.getElementById("sidebar"),null).getPropertyValue("left");
	if(value === "0px") {
		document.getElementById("sidebar").style.left = "-280px";
	} else {
		document.getElementById("sidebar").style.left = "0px";
	}
}

function goToSection(id) {
	let element = document.getElementById(id);
	element.scrollIntoView({behavior: "smooth", block: "end", inline: "start"});
	createEvent("GOTO-"+id);
}

function showContactEmail() {
	document.getElementById("contactEmail").style.display = null;
	window.location.href = "mailto:jobs@brandoncunningham.dev";
	createEvent("LINK_EMAIL");
}

//listeners for sidebar showing and hiding
minSizeForSidebar.addListener(handleMinSizeChange);
function handleMinSizeChange(e) {
  if (e.matches) {
  	console.log("MATCHES ON MIN");
  	document.getElementById("sidebar").style.left = "0px";
  }
}


maxSizeForSidebar.addListener(handleMaxSizeChange);
function handleMaxSizeChange(e) {
  if (e.matches) {
  	console.log("MATCHES ON MAX");
  	document.getElementById("sidebar").style.left = "-280px";
  }
}
//listeners for sidebar showing and hiding
//UI Stuff


//Contact form stuff
function submitContactForm() {
	createEvent("SUBMIT_CONTACT");
	var requestBody = validateInput();

	if(requestBody === null) {
		return;
	}

 	var http = new XMLHttpRequest();
 	var url = "https://brandoncunningham.dev/contact/"

 	http.open("POST", url, true);
 	http.setRequestHeader("Content-Type", "application/json");
 	http.setRequestHeader("sessionId", createOrGetSessionId());


	http.onreadystatechange = (response) => {
	  if (http.readyState === XMLHttpRequest.DONE) {
	  	if(http.status === 200) {
	  		document.getElementById("formSuccessMessage").innerText = "Message Sent!"; 
	  		createEvent("SUBMIT_CONTACT_SUCCESS");
	  	} else {
	  		document.getElementById("formErrorMessage").innerText = "Failed to submit contact request"; 
	  		createEvent("SUBMIT_CONTACT_FAILED");
	  	}
	  }
	}

 	http.send(JSON.stringify(requestBody));
}

function validateInput() {
	let subject = document.getElementById("subjectInput").value;
	let message = document.getElementById("messageInput").value;
	let email = document.getElementById("emailInput").value;

	document.getElementById("formErrorMessage").innerText = ""; 

	if(subject === null || subject === '') {
		document.getElementById("formErrorMessage").innerText = "Subject can not be empty"; 
		return null;
	} 

	if(message === null || message === '') {
		document.getElementById("formErrorMessage").innerText = "Message can not be empty"; 
		return null;
	}

	if(email === null || email === '') {
		document.getElementById("formErrorMessage").innerText = "Email can not be empty"; 
		return null;
	}


	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(!email.match(mailformat)) {
		document.getElementById("formErrorMessage").innerText = "Please enter a valid email"; 
		return null;
	}

	return requestBody = {
 		subject: subject,
 		message: message, 
 		contactEmail: email
 	};
}
//Contact form stuff



//analytics stuff
window.onload = function() {
	localStorage.removeItem("sessionId");
	createOrGetSessionId();
	createOrGetUserId();
	createEvent("PAGE_LOAD");
	addListeners();
}

function createEvent(eventTypeName) {
 	var http = new XMLHttpRequest();
 	var url = "https://brandoncunningham.dev/analytics/"

 	http.open("POST", url, true);
 	http.setRequestHeader("Content-Type", "application/json");
 	http.setRequestHeader("sessionId", createOrGetSessionId());
 	http.setRequestHeader("userId", createOrGetUserId());


 	let width = window.innerWidth;
 	let height = window.innerHeight;
 	let screenWidth = screen.width;
 	let screenHeight = screen.height;

 	let requestBody = {
 		action: eventTypeName,
 		screenWidth: screenWidth,
 		screenHeight: screenHeight,
 		viewportHeight: height,
 		viewportWidth: width
 	};

 	http.send(JSON.stringify(requestBody));
 	return true;
}

function createOrGetSessionId() {
	if(localStorage.getItem("sessionId") === null) {
		let sessionId = createUUID();
		localStorage.setItem("sessionId", sessionId);
	}
	return localStorage.getItem("sessionId");
}

function createOrGetUserId() {
	if(localStorage.getItem("userId") === null) {
		let userId = createUUID();
		localStorage.setItem("userId", userId);
	}
	return localStorage.getItem("userId");
}

function createUUID() {
   return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
   });
}

function addListeners() {
	let listeningIds = ["RESUME", "VIEW_BPPROCESSING", "VIEW_BLURAYS",
						"VIEW_BLURAYS_SOURCE", "VIEW_OLD", "VIEW_OLD_SOURCE",
						"VIEW_GITHUB_SOURCE", "VIEW_ANALYTICS",
						"LINK_LINKEDIN", "LINK_GITLAB", "LINK_GITHUB","LINK_FIGMA"];

	console.log("Adding listeners"); 

	for(let id of listeningIds) {
		document.getElementById(id).addEventListener('click', function() {createEvent(id);}, false);
	}
}

function liveCheck() {
	createEvent("KEEP_ALIVE");
}

setInterval(liveCheck, 30000); 
//analytics stuff






